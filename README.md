*This is my configs for django project firstly and common configs/guides for php-fpm, iptables, vsftpd and other things*

Readme v1.2.1.0
===============
This documentation location in main branch named 'master'.  
Adding new subparagraph, a,b,c,..., upper readme version on 0.0.0.1  
Adding new paragraph, 1,2,3,..., upper readme version on 0.0.1  
Adding new chapter, I,II,III, upper readme version on 0.1  

0. Docs-tree

    1. Project tree of documentation.
        * common (Folder include templates for configuration nginx virtualhosts)
            * default (Template. Standart location for virtualhosts)
            * php (Template. Rules parsing php-files for nginx
        * nginx (Configuration files for nginx web-server. Default folder path: /etc/nginx/)  
            * nginx.conf (Main configuration file Nginx)
        * php (Link to default PHP7 location in server: /etc/php/7.0/)
            * mods-available ()
                * memcached.ini (Configuration file wor work memcached, as a php module)
        * sites-available (Virtualhost configuration file: /etc/nginx/)
            * test.conf (Virtual host conf file)
        * django-vagrant (Confuguration files for django-app, web-server, etc)
            * nginx.conf (Nginx configuration for work with uWSGI)
            * requirements.txt (Depends for Django-project: psycopg2, Django)
            * ubuntu_vps.django.instruction (Instruction of settings up web-server and django-app)
            * wsgi.ini (uWSGI configuration)
        * iptables (Include iptables-commands)
        * links (Links to repositories, deb-packages)
        * php.7.0.png (PHP7 depends during install it)
        * postgresql (PostgreSQL install and settings up commands)
        * README.md (Documentation. You are here!)
        * settings (Commands for settings up nginx & php-apps)
        * vsftpd.conf (Commands for settings up vsftpd)
  
I. How to start use Git.

1. Registration.  
    a. Registration on any of git-hostings, like a Github or Bitbucket.  
    b. Create your repository.  
2. SSH-keys.  
    a.  Generate your ssh-keys  
        ```ssh-keygen```  
    b. Copy your public key to management panel of git-hosting.  
    c. Save your private key to ~/.ssh/<private_key_name>  
    d. Add your private key to git-bash.  
        ```  
        ssh-add ~/.ssh/<private_key_name>  
        # Check added private key  
        ssh-add -l  
        ```
3. Local repository.  
    a. Create local repository.  
        ```
        mkdir /some-path/to/git-folder/  
        cd /some-path/to/git-folder/  
        ```  
    b. Clone your remote repository  
        ```  
        git clone ssh://<link_to_git-repository>  
        ```  